﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="19008000">
	<Property Name="NI.Project.Description" Type="Str">This application was created to aid in documentation of a project.
&lt;b&gt; &lt;/b&gt; &lt;b&gt; &lt;/b&gt;
©2013-2019, Q Software Innovations, LLC.

Written in National Instruments LabVIEW™ 2019.

Authored by Quentin Alldredge
Q@qsoftwareinnovations.com
www.qsoftwareinnovations.com

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Common" Type="Folder">
			<Item Name="Fade In" Type="Folder">
				<Item Name="Fade In (Caller).vi" Type="VI" URL="../Common/Fade In/Fade In (Caller).vi"/>
				<Item Name="Fade In (Current).vi" Type="VI" URL="../Common/Fade In/Fade In (Current).vi"/>
				<Item Name="Fade In (Level).vi" Type="VI" URL="../Common/Fade In/Fade In (Level).vi"/>
				<Item Name="Fade In (Reference).vi" Type="VI" URL="../Common/Fade In/Fade In (Reference).vi"/>
				<Item Name="Fade In (Top).vi" Type="VI" URL="../Common/Fade In/Fade In (Top).vi"/>
			</Item>
			<Item Name="Fade Out" Type="Folder">
				<Item Name="Fade Out (Caller).vi" Type="VI" URL="../Common/Fade Out/Fade Out (Caller).vi"/>
				<Item Name="Fade Out (Current).vi" Type="VI" URL="../Common/Fade Out/Fade Out (Current).vi"/>
				<Item Name="Fade Out (Level).vi" Type="VI" URL="../Common/Fade Out/Fade Out (Level).vi"/>
				<Item Name="Fade Out (Reference).vi" Type="VI" URL="../Common/Fade Out/Fade Out (Reference).vi"/>
				<Item Name="Fade Out (Top).vi" Type="VI" URL="../Common/Fade Out/Fade Out (Top).vi"/>
			</Item>
			<Item Name="Pause UI Updates" Type="Folder">
				<Item Name="Pause UI Updates (Caller).vi" Type="VI" URL="../Common/Pause UI Updates/Pause UI Updates (Caller).vi"/>
				<Item Name="Pause UI Updates (Current).vi" Type="VI" URL="../Common/Pause UI Updates/Pause UI Updates (Current).vi"/>
				<Item Name="Pause UI Updates (Level).vi" Type="VI" URL="../Common/Pause UI Updates/Pause UI Updates (Level).vi"/>
				<Item Name="Pause UI Updates (Reference).vi" Type="VI" URL="../Common/Pause UI Updates/Pause UI Updates (Reference).vi"/>
				<Item Name="Pause UI Updates (Top).vi" Type="VI" URL="../Common/Pause UI Updates/Pause UI Updates (Top).vi"/>
			</Item>
			<Item Name="Start UI Updates" Type="Folder">
				<Item Name="Start UI Updates (Caller).vi" Type="VI" URL="../Common/Start UI Updates/Start UI Updates (Caller).vi"/>
				<Item Name="Start UI Updates (Current).vi" Type="VI" URL="../Common/Start UI Updates/Start UI Updates (Current).vi"/>
				<Item Name="Start UI Updates (Level).vi" Type="VI" URL="../Common/Start UI Updates/Start UI Updates (Level).vi"/>
				<Item Name="Start UI Updates (Reference).vi" Type="VI" URL="../Common/Start UI Updates/Start UI Updates (Reference).vi"/>
				<Item Name="Start UI Updates (Top).vi" Type="VI" URL="../Common/Start UI Updates/Start UI Updates (Top).vi"/>
			</Item>
			<Item Name="Calculate Popup Position.vi" Type="VI" URL="../Common/Calculate Popup Position.vi"/>
			<Item Name="Center Dialog in Caller.vi" Type="VI" URL="../Common/Center Dialog in Caller.vi"/>
			<Item Name="Fade In.vi" Type="VI" URL="../Common/Fade In.vi"/>
			<Item Name="Fade Out.vi" Type="VI" URL="../Common/Fade Out.vi"/>
			<Item Name="Get All Children.vi" Type="VI" URL="../Common/Get All Children.vi"/>
			<Item Name="Get All Descendents.vi" Type="VI" URL="../Common/Get All Descendents.vi"/>
			<Item Name="Get All Siblings.vi" Type="VI" URL="../Common/Get All Siblings.vi"/>
			<Item Name="Open Dialog Transparent.vi" Type="VI" URL="../Common/Open Dialog Transparent.vi"/>
			<Item Name="Pause UI Updates.vi" Type="VI" URL="../Common/Pause UI Updates.vi"/>
			<Item Name="Start UI Updates.vi" Type="VI" URL="../Common/Start UI Updates.vi"/>
			<Item Name="Warning Dialog.vi" Type="VI" URL="../Common/Warning Dialog.vi"/>
		</Item>
		<Item Name="CustomControls" Type="Folder">
			<Item Name="Adobe Reader Button.ctl" Type="VI" URL="../CustomControls/Adobe Reader Button.ctl"/>
			<Item Name="Apply Button.ctl" Type="VI" URL="../CustomControls/Apply Button.ctl"/>
			<Item Name="Bold Button.ctl" Type="VI" URL="../CustomControls/Bold Button.ctl"/>
			<Item Name="Cancel Button.ctl" Type="VI" URL="../CustomControls/Cancel Button.ctl"/>
			<Item Name="Copy Button.ctl" Type="VI" URL="../CustomControls/Copy Button.ctl"/>
			<Item Name="Cut Button.ctl" Type="VI" URL="../CustomControls/Cut Button.ctl"/>
			<Item Name="Doc Type.ctl" Type="VI" URL="../CustomControls/Doc Type.ctl"/>
			<Item Name="Include Controls Button.ctl" Type="VI" URL="../CustomControls/Include Controls Button.ctl"/>
			<Item Name="Info Button.ctl" Type="VI" URL="../CustomControls/Info Button.ctl"/>
			<Item Name="Insert Symbol Button.ctl" Type="VI" URL="../CustomControls/Insert Symbol Button.ctl"/>
			<Item Name="Large Transparent Button.ctl" Type="VI" URL="../CustomControls/Large Transparent Button.ctl"/>
			<Item Name="Lock-Unlock Button.ctl" Type="VI" URL="../CustomControls/Lock-Unlock Button.ctl"/>
			<Item Name="Paste Button.ctl" Type="VI" URL="../CustomControls/Paste Button.ctl"/>
			<Item Name="Project Button.ctl" Type="VI" URL="../CustomControls/Project Button.ctl"/>
			<Item Name="Project Explorer Window Button.ctl" Type="VI" URL="../CustomControls/Project Explorer Window Button.ctl"/>
			<Item Name="Redo Button.ctl" Type="VI" URL="../CustomControls/Redo Button.ctl"/>
			<Item Name="Refresh Button.ctl" Type="VI" URL="../CustomControls/Refresh Button.ctl"/>
			<Item Name="Select All Button.ctl" Type="VI" URL="../CustomControls/Select All Button.ctl"/>
			<Item Name="Small Refresh Button.ctl" Type="VI" URL="../CustomControls/Small Refresh Button.ctl"/>
			<Item Name="Undo Button.ctl" Type="VI" URL="../CustomControls/Undo Button.ctl"/>
		</Item>
		<Item Name="Help" Type="Folder">
			<Item Name="HTML Files" Type="Folder">
				<Item Name="Graphics" Type="Folder">
					<Item Name="AboutScreen.pdn" Type="Document" URL="../Help/HTML Files/Graphics/AboutScreen.pdn"/>
					<Item Name="AboutScreen.png" Type="Document" URL="../Help/HTML Files/Graphics/AboutScreen.png"/>
					<Item Name="BoldButton.png" Type="Document" URL="../Help/HTML Files/Graphics/BoldButton.png"/>
					<Item Name="BoldInsertMenu.png" Type="Document" URL="../Help/HTML Files/Graphics/BoldInsertMenu.png"/>
					<Item Name="CommonDoc.png" Type="Document" URL="../Help/HTML Files/Graphics/CommonDoc.png"/>
					<Item Name="CutCopyPasteMenu.png" Type="Document" URL="../Help/HTML Files/Graphics/CutCopyPasteMenu.png"/>
					<Item Name="DocEditor.png" Type="Document" URL="../Help/HTML Files/Graphics/DocEditor.png"/>
					<Item Name="DocEditorToolbar.png" Type="Document" URL="../Help/HTML Files/Graphics/DocEditorToolbar.png"/>
					<Item Name="DocEditorToolbar2.png" Type="Document" URL="../Help/HTML Files/Graphics/DocEditorToolbar2.png"/>
					<Item Name="DocEditorToolbar3.png" Type="Document" URL="../Help/HTML Files/Graphics/DocEditorToolbar3.png"/>
					<Item Name="DocEditorToolbar4.png" Type="Document" URL="../Help/HTML Files/Graphics/DocEditorToolbar4.png"/>
					<Item Name="DocEditorToolbar5.png" Type="Document" URL="../Help/HTML Files/Graphics/DocEditorToolbar5.png"/>
					<Item Name="DocEditorToolbar6.png" Type="Document" URL="../Help/HTML Files/Graphics/DocEditorToolbar6.png"/>
					<Item Name="DocEditorToolbar7.png" Type="Document" URL="../Help/HTML Files/Graphics/DocEditorToolbar7.png"/>
					<Item Name="DocEditorToolbar8.png" Type="Document" URL="../Help/HTML Files/Graphics/DocEditorToolbar8.png"/>
					<Item Name="ExitMenu.png" Type="Document" URL="../Help/HTML Files/Graphics/ExitMenu.png"/>
					<Item Name="IconView.png" Type="Document" URL="../Help/HTML Files/Graphics/IconView.png"/>
					<Item Name="InsertButton.png" Type="Document" URL="../Help/HTML Files/Graphics/InsertButton.png"/>
					<Item Name="Main.png" Type="Document" URL="../Help/HTML Files/Graphics/Main.png"/>
					<Item Name="MainEditorParts.pdn" Type="Document" URL="../Help/HTML Files/Graphics/MainEditorParts.pdn"/>
					<Item Name="MainEditorParts.png" Type="Document" URL="../Help/HTML Files/Graphics/MainEditorParts.png"/>
					<Item Name="OpenProjectButton.png" Type="Document" URL="../Help/HTML Files/Graphics/OpenProjectButton.png"/>
					<Item Name="OpenProjectExplorerButton.png" Type="Document" URL="../Help/HTML Files/Graphics/OpenProjectExplorerButton.png"/>
					<Item Name="OpenProjectExplorerMenu.png" Type="Document" URL="../Help/HTML Files/Graphics/OpenProjectExplorerMenu.png"/>
					<Item Name="OpenProjectMenu.png" Type="Document" URL="../Help/HTML Files/Graphics/OpenProjectMenu.png"/>
					<Item Name="OptionsDialog.png" Type="Document" URL="../Help/HTML Files/Graphics/OptionsDialog.png"/>
					<Item Name="PreviewContextHelp.png" Type="Document" URL="../Help/HTML Files/Graphics/PreviewContextHelp.png"/>
					<Item Name="ProjectExplorer.png" Type="Document" URL="../Help/HTML Files/Graphics/ProjectExplorer.png"/>
					<Item Name="ProjectExplorerToolbar.png" Type="Document" URL="../Help/HTML Files/Graphics/ProjectExplorerToolbar.png"/>
					<Item Name="ProjectExplorerToolbar2.png" Type="Document" URL="../Help/HTML Files/Graphics/ProjectExplorerToolbar2.png"/>
					<Item Name="RefreshButton.png" Type="Document" URL="../Help/HTML Files/Graphics/RefreshButton.png"/>
					<Item Name="RefreshMenu.png" Type="Document" URL="../Help/HTML Files/Graphics/RefreshMenu.png"/>
					<Item Name="RevertButton.png" Type="Document" URL="../Help/HTML Files/Graphics/RevertButton.png"/>
					<Item Name="SaveButton.png" Type="Document" URL="../Help/HTML Files/Graphics/SaveButton.png"/>
					<Item Name="SaveCommonPrompt.png" Type="Document" URL="../Help/HTML Files/Graphics/SaveCommonPrompt.png"/>
					<Item Name="SaveRevertMenu.png" Type="Document" URL="../Help/HTML Files/Graphics/SaveRevertMenu.png"/>
					<Item Name="ShowPreviewMenu.png" Type="Document" URL="../Help/HTML Files/Graphics/ShowPreviewMenu.png"/>
					<Item Name="SpecialCharacterPopup.png" Type="Document" URL="../Help/HTML Files/Graphics/SpecialCharacterPopup.png"/>
					<Item Name="SpecificDoc.png" Type="Document" URL="../Help/HTML Files/Graphics/SpecificDoc.png"/>
					<Item Name="Tools-OptionsMenu.png" Type="Document" URL="../Help/HTML Files/Graphics/Tools-OptionsMenu.png"/>
					<Item Name="UndoRedoMenu.png" Type="Document" URL="../Help/HTML Files/Graphics/UndoRedoMenu.png"/>
				</Item>
				<Item Name="Editing Documentation.htm" Type="Document" URL="../Help/HTML Files/Editing Documentation.htm"/>
				<Item Name="Introduction.htm" Type="Document" URL="../Help/HTML Files/Introduction.htm"/>
				<Item Name="Main Editor Window.htm" Type="Document" URL="../Help/HTML Files/Main Editor Window.htm"/>
				<Item Name="Navigating a Project.htm" Type="Document" URL="../Help/HTML Files/Navigating a Project.htm"/>
				<Item Name="Opening a Project.htm" Type="Document" URL="../Help/HTML Files/Opening a Project.htm"/>
				<Item Name="Opening the Preview Context Help.htm" Type="Document" URL="../Help/HTML Files/Opening the Preview Context Help.htm"/>
				<Item Name="Options Dialog.htm" Type="Document" URL="../Help/HTML Files/Options Dialog.htm"/>
				<Item Name="Purchasing.htm" Type="Document" URL="../Help/HTML Files/Purchasing.htm"/>
				<Item Name="Saving Documentation.htm" Type="Document" URL="../Help/HTML Files/Saving Documentation.htm"/>
				<Item Name="Support.htm" Type="Document" URL="../Help/HTML Files/Support.htm"/>
			</Item>
			<Item Name="Documentation Editor Help.chm" Type="Document" URL="../Help/Documentation Editor Help.chm"/>
			<Item Name="Documentation Editor Help.chw" Type="Document" URL="../Help/Documentation Editor Help.chw"/>
			<Item Name="Documentation Editor Help.docx" Type="Document" URL="../Help/Documentation Editor Help.docx"/>
			<Item Name="Documentation Editor Help.hhp" Type="Document" URL="../Help/Documentation Editor Help.hhp"/>
			<Item Name="Index.hhk" Type="Document" URL="../Help/Index.hhk"/>
			<Item Name="Open Help.vi" Type="VI" URL="../Help/Open Help.vi"/>
			<Item Name="Table of Contents.hhc" Type="Document" URL="../Help/Table of Contents.hhc"/>
		</Item>
		<Item Name="Menus" Type="Folder">
			<Item Name="MainMenu.rtm" Type="Document" URL="../Menus/MainMenu.rtm"/>
			<Item Name="PreviewHelpMenu.rtm" Type="Document" URL="../Menus/PreviewHelpMenu.rtm"/>
		</Item>
		<Item Name="SubVIs" Type="Folder">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="Commands" Type="Folder">
				<Item Name="DE Command Key Down.vi" Type="VI" URL="../SubVIs/Commands/DE Command Key Down.vi"/>
				<Item Name="DE Command Mouse Down.vi" Type="VI" URL="../SubVIs/Commands/DE Command Mouse Down.vi"/>
				<Item Name="DE Command Mouse Up.vi" Type="VI" URL="../SubVIs/Commands/DE Command Mouse Up.vi"/>
			</Item>
			<Item Name="PEX Interaction" Type="Folder">
				<Item Name="Get Item Info.vi" Type="VI" URL="../SubVIs/PEX Interaction/Get Item Info.vi"/>
			</Item>
			<Item Name="Preview" Type="Folder">
				<Item Name="DE Preview Apply Formatting.vi" Type="VI" URL="../SubVIs/Preview/DE Preview Apply Formatting.vi"/>
				<Item Name="DE Preview Calculated Description Height.vi" Type="VI" URL="../SubVIs/Preview/DE Preview Calculated Description Height.vi"/>
				<Item Name="DE Preview Dialog.vi" Type="VI" URL="../SubVIs/Preview/DE Preview Dialog.vi"/>
				<Item Name="DE Preview Get All Info.vi" Type="VI" URL="../SubVIs/Preview/DE Preview Get All Info.vi"/>
				<Item Name="DE Preview Get Description Only.vi" Type="VI" URL="../SubVIs/Preview/DE Preview Get Description Only.vi"/>
				<Item Name="DE Preview Icon Size.vi" Type="VI" URL="../SubVIs/Preview/DE Preview Icon Size.vi"/>
				<Item Name="DE Preview Largest Width.vi" Type="VI" URL="../SubVIs/Preview/DE Preview Largest Width.vi"/>
				<Item Name="DE Preview Name.vi" Type="VI" URL="../SubVIs/Preview/DE Preview Name.vi"/>
				<Item Name="DE Preview New Item.vi" Type="VI" URL="../SubVIs/Preview/DE Preview New Item.vi"/>
				<Item Name="DE Preview Open.vi" Type="VI" URL="../SubVIs/Preview/DE Preview Open.vi"/>
				<Item Name="DE Preview Set Description.vi" Type="VI" URL="../SubVIs/Preview/DE Preview Set Description.vi"/>
				<Item Name="DE Preview Set Dialog.vi" Type="VI" URL="../SubVIs/Preview/DE Preview Set Dialog.vi"/>
				<Item Name="DE Preview Set Icon.vi" Type="VI" URL="../SubVIs/Preview/DE Preview Set Icon.vi"/>
				<Item Name="DE Preview Set Name.vi" Type="VI" URL="../SubVIs/Preview/DE Preview Set Name.vi"/>
				<Item Name="DE Preview Update Description.vi" Type="VI" URL="../SubVIs/Preview/DE Preview Update Description.vi"/>
			</Item>
			<Item Name="Text Editor" Type="Folder">
				<Item Name="TE Apply Chars[] to Editor.vi" Type="VI" URL="../SubVIs/Text Editor/TE Apply Chars[] to Editor.vi"/>
				<Item Name="TE Backspace Characters.vi" Type="VI" URL="../SubVIs/Text Editor/TE Backspace Characters.vi"/>
				<Item Name="TE Chars[] to Tagged String.vi" Type="VI" URL="../SubVIs/Text Editor/TE Chars[] to Tagged String.vi"/>
				<Item Name="TE Copy.vi" Type="VI" URL="../SubVIs/Text Editor/TE Copy.vi"/>
				<Item Name="TE Cut.vi" Type="VI" URL="../SubVIs/Text Editor/TE Cut.vi"/>
				<Item Name="TE Delete Characters.vi" Type="VI" URL="../SubVIs/Text Editor/TE Delete Characters.vi"/>
				<Item Name="TE Insert Character.vi" Type="VI" URL="../SubVIs/Text Editor/TE Insert Character.vi"/>
				<Item Name="TE Key Down.vi" Type="VI" URL="../SubVIs/Text Editor/TE Key Down.vi"/>
				<Item Name="TE Mouse Down.vi" Type="VI" URL="../SubVIs/Text Editor/TE Mouse Down.vi"/>
				<Item Name="TE Mouse Up.vi" Type="VI" URL="../SubVIs/Text Editor/TE Mouse Up.vi"/>
				<Item Name="TE Paste.vi" Type="VI" URL="../SubVIs/Text Editor/TE Paste.vi"/>
				<Item Name="TE Select All.vi" Type="VI" URL="../SubVIs/Text Editor/TE Select All.vi"/>
				<Item Name="TE Select Mode.vi" Type="VI" URL="../SubVIs/Text Editor/TE Select Mode.vi"/>
				<Item Name="TE Set Bold Selection.vi" Type="VI" URL="../SubVIs/Text Editor/TE Set Bold Selection.vi"/>
				<Item Name="TE Set Bold Status.vi" Type="VI" URL="../SubVIs/Text Editor/TE Set Bold Status.vi"/>
				<Item Name="TE Tagged String to Chars[].vi" Type="VI" URL="../SubVIs/Text Editor/TE Tagged String to Chars[].vi"/>
			</Item>
			<Item Name="Undo-Redo" Type="Folder">
				<Item Name="DE Add to Undo Queue.vi" Type="VI" URL="../SubVIs/Undo-Redo/DE Add to Undo Queue.vi"/>
				<Item Name="DE End Segment.vi" Type="VI" URL="../SubVIs/Undo-Redo/DE End Segment.vi"/>
				<Item Name="DE Flush Undo-Redo Queue.vi" Type="VI" URL="../SubVIs/Undo-Redo/DE Flush Undo-Redo Queue.vi"/>
				<Item Name="DE Redo.vi" Type="VI" URL="../SubVIs/Undo-Redo/DE Redo.vi"/>
				<Item Name="DE Undo.vi" Type="VI" URL="../SubVIs/Undo-Redo/DE Undo.vi"/>
			</Item>
			<Item Name="DE 8 Warning Message.vi" Type="VI" URL="../SubVIs/DE 8 Warning Message.vi"/>
			<Item Name="DE 1562 Warning Message.vi" Type="VI" URL="../SubVIs/DE 1562 Warning Message.vi"/>
			<Item Name="DE About Dialog.vi" Type="VI" URL="../SubVIs/DE About Dialog.vi"/>
			<Item Name="DE Add-Remove Key Binding.vi" Type="VI" URL="../SubVIs/DE Add-Remove Key Binding.vi"/>
			<Item Name="DE Bold.vi" Type="VI" URL="../SubVIs/DE Bold.vi"/>
			<Item Name="DE Change Doc Type.vi" Type="VI" URL="../SubVIs/DE Change Doc Type.vi"/>
			<Item Name="DE Check File Permissions.vi" Type="VI" URL="../SubVIs/DE Check File Permissions.vi"/>
			<Item Name="DE Close Control References.vi" Type="VI" URL="../SubVIs/DE Close Control References.vi"/>
			<Item Name="DE Combine Description.vi" Type="VI" URL="../SubVIs/DE Combine Description.vi"/>
			<Item Name="DE Control Menu Items.vi" Type="VI" URL="../SubVIs/DE Control Menu Items.vi"/>
			<Item Name="DE Control Toolbar Items.vi" Type="VI" URL="../SubVIs/DE Control Toolbar Items.vi"/>
			<Item Name="DE Control UI Items.vi" Type="VI" URL="../SubVIs/DE Control UI Items.vi"/>
			<Item Name="DE Create Description.vi" Type="VI" URL="../SubVIs/DE Create Description.vi"/>
			<Item Name="DE Data Changed.vi" Type="VI" URL="../SubVIs/DE Data Changed.vi"/>
			<Item Name="DE Dequeue Command.vi" Type="VI" URL="../SubVIs/DE Dequeue Command.vi"/>
			<Item Name="DE Enqueue Command.vi" Type="VI" URL="../SubVIs/DE Enqueue Command.vi"/>
			<Item Name="DE Enqueue Error.vi" Type="VI" URL="../SubVIs/DE Enqueue Error.vi"/>
			<Item Name="DE Error Handler.vi" Type="VI" URL="../SubVIs/DE Error Handler.vi"/>
			<Item Name="DE Generic Warning Message.vi" Type="VI" URL="../SubVIs/DE Generic Warning Message.vi"/>
			<Item Name="DE Get Project Path.vi" Type="VI" URL="../SubVIs/DE Get Project Path.vi"/>
			<Item Name="DE Get User Event Refnum.vi" Type="VI" URL="../SubVIs/DE Get User Event Refnum.vi"/>
			<Item Name="DE Initialize Application.vi" Type="VI" URL="../SubVIs/DE Initialize Application.vi"/>
			<Item Name="DE Initialize Control References.vi" Type="VI" URL="../SubVIs/DE Initialize Control References.vi"/>
			<Item Name="DE Initialize UI.vi" Type="VI" URL="../SubVIs/DE Initialize UI.vi"/>
			<Item Name="DE Insert Character.vi" Type="VI" URL="../SubVIs/DE Insert Character.vi"/>
			<Item Name="DE Open Description.vi" Type="VI" URL="../SubVIs/DE Open Description.vi"/>
			<Item Name="DE Open Project Explorer Window.vi" Type="VI" URL="../SubVIs/DE Open Project Explorer Window.vi"/>
			<Item Name="DE Open Project.vi" Type="VI" URL="../SubVIs/DE Open Project.vi"/>
			<Item Name="DE Pause Updates.vi" Type="VI" URL="../SubVIs/DE Pause Updates.vi"/>
			<Item Name="DE Refresh Project.vi" Type="VI" URL="../SubVIs/DE Refresh Project.vi"/>
			<Item Name="DE Revert.vi" Type="VI" URL="../SubVIs/DE Revert.vi"/>
			<Item Name="DE Safe Shutdown.vi" Type="VI" URL="../SubVIs/DE Safe Shutdown.vi"/>
			<Item Name="DE Save State Change.vi" Type="VI" URL="../SubVIs/DE Save State Change.vi"/>
			<Item Name="DE Save.vi" Type="VI" URL="../SubVIs/DE Save.vi"/>
			<Item Name="DE Set Delimiter Tag.vi" Type="VI" URL="../SubVIs/DE Set Delimiter Tag.vi"/>
			<Item Name="DE Set Title Bar.vi" Type="VI" URL="../SubVIs/DE Set Title Bar.vi"/>
			<Item Name="DE Settings Dialog.vi" Type="VI" URL="../SubVIs/DE Settings Dialog.vi"/>
			<Item Name="DE Special Characters Dialog.vi" Type="VI" URL="../SubVIs/DE Special Characters Dialog.vi"/>
			<Item Name="DE Split Description.vi" Type="VI" URL="../SubVIs/DE Split Description.vi"/>
			<Item Name="DE Start Updates.vi" Type="VI" URL="../SubVIs/DE Start Updates.vi"/>
			<Item Name="DE Unsaved Item Check.vi" Type="VI" URL="../SubVIs/DE Unsaved Item Check.vi"/>
			<Item Name="DE Update UI.vi" Type="VI" URL="../SubVIs/DE Update UI.vi"/>
			<Item Name="DE Write Error Handler.vi" Type="VI" URL="../SubVIs/DE Write Error Handler.vi"/>
			<Item Name="Get Active Project Path.vi" Type="VI" URL="../SubVIs/Get Active Project Path.vi"/>
			<Item Name="Get Recent.vi" Type="VI" URL="../SubVIs/Get Recent.vi"/>
			<Item Name="Initialize Complete Event.vi" Type="VI" URL="../SubVIs/Initialize Complete Event.vi"/>
			<Item Name="License Check.vi" Type="VI" URL="../SubVIs/License Check.vi"/>
			<Item Name="Load Recently Opened Menu.vi" Type="VI" URL="../SubVIs/Load Recently Opened Menu.vi"/>
			<Item Name="Open Project on Startup.vi" Type="VI" URL="../SubVIs/Open Project on Startup.vi"/>
			<Item Name="Open Recent Project.vi" Type="VI" URL="../SubVIs/Open Recent Project.vi"/>
			<Item Name="Options Dialog.vi" Type="VI" URL="../SubVIs/Options Dialog.vi"/>
			<Item Name="Reset Recently Opened Menu.vi" Type="VI" URL="../SubVIs/Reset Recently Opened Menu.vi"/>
			<Item Name="Retrieve Persistant Data.vi" Type="VI" URL="../SubVIs/Retrieve Persistant Data.vi"/>
			<Item Name="Save Persistant Data.vi" Type="VI" URL="../SubVIs/Save Persistant Data.vi"/>
			<Item Name="Update Recent Menu.vi" Type="VI" URL="../ExampleProject/Update Recent Menu.vi"/>
		</Item>
		<Item Name="TypeDefs" Type="Folder">
			<Item Name="DE Command Cluster.ctl" Type="VI" URL="../TypeDefs/DE Command Cluster.ctl"/>
			<Item Name="DE Command Enum.ctl" Type="VI" URL="../TypeDefs/DE Command Enum.ctl"/>
			<Item Name="DE Control Cluster.ctl" Type="VI" URL="../TypeDefs/DE Control Cluster.ctl"/>
			<Item Name="DE Documentation Type Enum.ctl" Type="VI" URL="../TypeDefs/DE Documentation Type Enum.ctl"/>
			<Item Name="DE Key Definition Cluster.ctl" Type="VI" URL="../TypeDefs/DE Key Definition Cluster.ctl"/>
			<Item Name="DE Mouse Down Info.ctl" Type="VI" URL="../TypeDefs/DE Mouse Down Info.ctl"/>
			<Item Name="DE Preview Data Cluster.ctl" Type="VI" URL="../TypeDefs/DE Preview Data Cluster.ctl"/>
			<Item Name="DE Preview Dialog Refnums.ctl" Type="VI" URL="../TypeDefs/DE Preview Dialog Refnums.ctl"/>
			<Item Name="DE Preview Dialog State.ctl" Type="VI" URL="../TypeDefs/DE Preview Dialog State.ctl"/>
			<Item Name="DE Reference Cluster.ctl" Type="VI" URL="../TypeDefs/DE Reference Cluster.ctl"/>
			<Item Name="DE Special Characters.ctl" Type="VI" URL="../TypeDefs/DE Special Characters.ctl"/>
			<Item Name="DE State Cluster.ctl" Type="VI" URL="../TypeDefs/DE State Cluster.ctl"/>
			<Item Name="DE UI States.ctl" Type="VI" URL="../TypeDefs/DE UI States.ctl"/>
			<Item Name="DE Undo-Redo Information.ctl" Type="VI" URL="../TypeDefs/DE Undo-Redo Information.ctl"/>
			<Item Name="DE User Event References.ctl" Type="VI" URL="../TypeDefs/DE User Event References.ctl"/>
			<Item Name="Options Cluster.ctl" Type="VI" URL="../TypeDefs/Options Cluster.ctl"/>
			<Item Name="Persistant Data Cluster.ctl" Type="VI" URL="../TypeDefs/Persistant Data Cluster.ctl"/>
			<Item Name="TE Character Information.ctl" Type="VI" URL="../TypeDefs/TE Character Information.ctl"/>
		</Item>
		<Item Name="Documentation Editor Main.vi" Type="VI" URL="../Documentation Editor Main.vi"/>
		<Item Name="Project Explorer XControl.xctl" Type="XControl" URL="../Project Explorer XControl/Project Explorer XControl.xctl"/>
		<Item Name="Release Notes.pdf" Type="Document" URL="../Release Notes.pdf"/>
		<Item Name="Splash Screen.vi" Type="VI" URL="../Splash Screen.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="MGI Create Directory Chain Behavior Enum.ctl" Type="VI" URL="/&lt;userlib&gt;/_MGI/File/MGI Create Directory Chain Behavior Enum.ctl"/>
				<Item Name="MGI Create Directory Chain.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/File/MGI Create Directory Chain.vi"/>
				<Item Name="MGI Get Cluster Elements.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Cluster/MGI Get Cluster Elements.vi"/>
				<Item Name="MGI Hex Str to U8 Data.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/String/MGI Hex Str to U8 Data.vi"/>
				<Item Name="MGI Insert Reserved Error.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Error Handling/MGI Insert Reserved Error.vi"/>
				<Item Name="MGI Read Anything.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI Read Anything.vi"/>
				<Item Name="MGI RWA Anything to String.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Anything to String.vi"/>
				<Item Name="MGI RWA Build Array Name.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Build Array Name.vi"/>
				<Item Name="MGI RWA Build Line.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Build Line.vi"/>
				<Item Name="MGI RWA Convertion Direction Enum.ctl" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Convertion Direction Enum.ctl"/>
				<Item Name="MGI RWA Enque Top Level Data.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Enque Top Level Data.vi"/>
				<Item Name="MGI RWA Get Type Info.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Get Type Info.vi"/>
				<Item Name="MGI RWA Handle Tag or Refnum.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Handle Tag or Refnum.vi"/>
				<Item Name="MGI RWA INI Tag Lookup.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA INI Tag Lookup.vi"/>
				<Item Name="MGI RWA Options Cluster.ctl" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Options Cluster.ctl"/>
				<Item Name="MGI RWA Process Array Elements.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Process Array Elements.vi"/>
				<Item Name="MGI RWA Read Strings from File.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Read Strings from File.vi"/>
				<Item Name="MGI RWA Remove EOLs and Slashes.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Remove EOLs and Slashes.vi"/>
				<Item Name="MGI RWA Replace Characters.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Replace Characters.vi"/>
				<Item Name="MGI RWA String To Anything.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA String To Anything.vi"/>
				<Item Name="MGI RWA Tag Lookup Cluster.ctl" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Tag Lookup Cluster.ctl"/>
				<Item Name="MGI RWA Unprocess Array Elements.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Unprocess Array Elements.vi"/>
				<Item Name="MGI RWA Unreplace Characters.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Unreplace Characters.vi"/>
				<Item Name="MGI RWA Write Strings to File.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Write Strings to File.vi"/>
				<Item Name="MGI Scan From String (CDB).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/String/MGI Scan From String/MGI Scan From String (CDB).vi"/>
				<Item Name="MGI Scan From String (CDB[]).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/String/MGI Scan From String/MGI Scan From String (CDB[]).vi"/>
				<Item Name="MGI Scan From String (CSG).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/String/MGI Scan From String/MGI Scan From String (CSG).vi"/>
				<Item Name="MGI Scan From String (CSG[]).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/String/MGI Scan From String/MGI Scan From String (CSG[]).vi"/>
				<Item Name="MGI Scan From String (CXT).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/String/MGI Scan From String/MGI Scan From String (CXT).vi"/>
				<Item Name="MGI Scan From String (CXT[]).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/String/MGI Scan From String/MGI Scan From String (CXT[]).vi"/>
				<Item Name="MGI Scan From String (DBL[]).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/String/MGI Scan From String/MGI Scan From String (DBL[]).vi"/>
				<Item Name="MGI Scan From String.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/String/MGI Scan From String.vi"/>
				<Item Name="MGI Suppress Error Code (Array).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Error Handling/MGI Suppress Error Code/MGI Suppress Error Code (Array).vi"/>
				<Item Name="MGI Suppress Error Code (Scalar).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Error Handling/MGI Suppress Error Code/MGI Suppress Error Code (Scalar).vi"/>
				<Item Name="MGI Suppress Error Code.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Error Handling/MGI Suppress Error Code.vi"/>
				<Item Name="MGI U8 Data to Hex Str.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/String/MGI U8 Data to Hex Str.vi"/>
				<Item Name="MGI Windows Get Regional String.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/String/MGI Windows Get Regional String.vi"/>
				<Item Name="MGI Windows Regional Ring.ctl" Type="VI" URL="/&lt;userlib&gt;/_MGI/String/MGI Windows Get Regional String/MGI Windows Regional Ring.ctl"/>
				<Item Name="MGI Write Anything.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI Write Anything.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Draw Flattened Pixmap.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Flattened Pixmap.vi"/>
				<Item Name="Draw Multiple Lines.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Multiple Lines.vi"/>
				<Item Name="Draw Text at Point.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Text at Point.vi"/>
				<Item Name="Draw Text in Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Text in Rect.vi"/>
				<Item Name="DTbl Empty Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Empty Digital.vi"/>
				<Item Name="DWDT Empty Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Empty Digital.vi"/>
				<Item Name="Empty Picture" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Empty Picture"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="Escape Characters for HTTP.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Escape Characters for HTTP.vi"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="FixBadRect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/FixBadRect.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVKeyTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVKeyTypeDef.ctl"/>
				<Item Name="LVNumericRepresentation.ctl" Type="VI" URL="/&lt;vilib&gt;/numeric/LVNumericRepresentation.ctl"/>
				<Item Name="LVPoint32TypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPoint32TypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="LVRowAndColumnTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRowAndColumnTypeDef.ctl"/>
				<Item Name="LVRowAndColumnUnsignedTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRowAndColumnUnsignedTypeDef.ctl"/>
				<Item Name="LVSelectionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVSelectionTypeDef.ctl"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Open URL in Default Browser (path).vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser (path).vi"/>
				<Item Name="Open URL in Default Browser (string).vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser (string).vi"/>
				<Item Name="Open URL in Default Browser core.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser core.vi"/>
				<Item Name="Open URL in Default Browser.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser.vi"/>
				<Item Name="Path to URL inner.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Path to URL inner.vi"/>
				<Item Name="Path to URL.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Path to URL.vi"/>
				<Item Name="PCT Pad String.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/PCT Pad String.vi"/>
				<Item Name="Picture to Pixmap.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/Picture to Pixmap.vi"/>
				<Item Name="Recursive File List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Recursive File List.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set Pen State.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Set Pen State.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="Version To Dotted String.vi" Type="VI" URL="/&lt;vilib&gt;/_xctls/Version To Dotted String.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="XControlSupport.lvlib" Type="Library" URL="/&lt;vilib&gt;/_xctls/XControlSupport.lvlib"/>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="Documentation Editor" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{9A6D381D-17E5-4189-8823-702527F992CB}</Property>
				<Property Name="App_INI_GUID" Type="Str">{36C7DCA6-75BB-49B3-99B4-5ACBC0EDA1A0}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{5D538279-7DC4-4560-83A2-4556948FB595}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Documentation Editor</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/Documentation Editor</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{035B39D6-D96D-4A46-A4E8-613C9F2AF8B3}</Property>
				<Property Name="Bld_version.build" Type="Int">3</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">DocEditor.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/Documentation Editor/DocEditor.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/Documentation Editor/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{2ADBC479-E41D-45C4-AA8B-5BA1CF3D4310}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Documentation Editor Main.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref"></Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="SourceCount" Type="Int">3</Property>
				<Property Name="TgtF_companyName" Type="Str">Q Software Innovations, LLC</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Documentation Editor</Property>
				<Property Name="TgtF_internalName" Type="Str">Documentation Editor</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2013 Q Software Innovations, LLC</Property>
				<Property Name="TgtF_productName" Type="Str">Documentation Editor</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{F796CBF3-3CC4-4751-9D32-12590EBCCFDC}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">DocEditor.exe</Property>
			</Item>
			<Item Name="QSI Documentation Editor PPL" Type="Packed Library">
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{D73C941B-4647-42ED-AE5F-7C3FF0E56B57}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">QSI Documentation Editor PPL</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/QSI Documentation Editor PPL</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{D2F4F7CB-34E8-4D8E-862C-A8AF238705F5}</Property>
				<Property Name="Bld_version.build" Type="Int">7</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">QSI_DE_CORE.lvlibp</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/QSI Documentation Editor PPL/QSI_DE_CORE.lvlibp</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/QSI Documentation Editor PPL</Property>
				<Property Name="Destination[2].destName" Type="Str">Help</Property>
				<Property Name="Destination[2].path" Type="Path">../builds/NI_AB_PROJECTNAME/QSI Documentation Editor PPL/Help</Property>
				<Property Name="DestinationCount" Type="Int">3</Property>
				<Property Name="PackedLib_callersAdapt" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{39B84DCF-60DB-47A2-8235-A5E562C9B9CD}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref"></Property>
				<Property Name="Source[1].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[1].Library.atomicCopy" Type="Bool">true</Property>
				<Property Name="Source[1].Library.LVLIBPtopLevel" Type="Bool">true</Property>
				<Property Name="Source[1].preventRename" Type="Bool">true</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">Library</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Release Notes.pdf</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Help/Open Help.vi</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].type" Type="Str">VI</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[4].itemID" Type="Ref"></Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[5].itemID" Type="Ref"></Property>
				<Property Name="Source[5].properties[0].type" Type="Str">Run when opened</Property>
				<Property Name="Source[5].properties[0].value" Type="Bool">true</Property>
				<Property Name="Source[5].properties[1].type" Type="Str">Remove front panel</Property>
				<Property Name="Source[5].properties[1].value" Type="Bool">false</Property>
				<Property Name="Source[5].properties[2].type" Type="Str">Remove block diagram</Property>
				<Property Name="Source[5].properties[2].value" Type="Bool">true</Property>
				<Property Name="Source[5].properties[3].type" Type="Str">Allow debugging</Property>
				<Property Name="Source[5].properties[3].value" Type="Bool">false</Property>
				<Property Name="Source[5].properties[4].type" Type="Str">Auto error handling</Property>
				<Property Name="Source[5].properties[4].value" Type="Bool">false</Property>
				<Property Name="Source[5].propertiesCount" Type="Int">5</Property>
				<Property Name="Source[5].type" Type="Str">VI</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/Documentation Editor Main.vi</Property>
				<Property Name="Source[6].properties[0].type" Type="Str">Run when opened</Property>
				<Property Name="Source[6].properties[0].value" Type="Bool">true</Property>
				<Property Name="Source[6].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[6].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">7</Property>
				<Property Name="TgtF_companyName" Type="Str">Q Software Innovations, LLC</Property>
				<Property Name="TgtF_fileDescription" Type="Str">QSI Documentation Editor PPL</Property>
				<Property Name="TgtF_internalName" Type="Str">QSI Documentation Editor PPL</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2013 Q Software Innovations, LLC</Property>
				<Property Name="TgtF_productName" Type="Str">QSI Documentation Editor PPL</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{8EBAFAC6-6C3D-43AD-9BCE-37C19968060E}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">QSI_DE_CORE.lvlibp</Property>
			</Item>
			<Item Name="Sale Distribution" Type="Source Distribution">
				<Property Name="Bld_buildCacheID" Type="Str">{7E1151D3-73F3-41F0-BFDE-FAA881999957}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Sale Distribution</Property>
				<Property Name="Bld_excludedDirectory[0]" Type="Path">instr.lib</Property>
				<Property Name="Bld_excludedDirectory[0].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectoryCount" Type="Int">1</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/Sale Distribution</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{23F2C78C-A36E-42FA-B0B9-80E1C020EA44}</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Destination Directory</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/Sale Distribution</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/Sale Distribution/data</Property>
				<Property Name="Destination[2].destName" Type="Str">DE</Property>
				<Property Name="Destination[2].libraryName" Type="Str">DE.lvlib</Property>
				<Property Name="Destination[2].path" Type="Path">../builds/NI_AB_PROJECTNAME/Sale Distribution/_DE</Property>
				<Property Name="Destination[3].destName" Type="Str">Help</Property>
				<Property Name="Destination[3].path" Type="Path">../builds/NI_AB_PROJECTNAME/Sale Distribution/Help</Property>
				<Property Name="Destination[4].destName" Type="Str">Dependencies</Property>
				<Property Name="Destination[4].path" Type="Path">../builds/NI_AB_PROJECTNAME/Sale Distribution/_Dependencies</Property>
				<Property Name="DestinationCount" Type="Int">5</Property>
				<Property Name="Source[0].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[0].Container.applyPassword" Type="Bool">true</Property>
				<Property Name="Source[0].Container.applyPrefix" Type="Bool">true</Property>
				<Property Name="Source[0].destinationIndex" Type="Int">4</Property>
				<Property Name="Source[0].itemID" Type="Str">{38343135-DDB1-4472-9BF1-40C44DD4D771}</Property>
				<Property Name="Source[0].newName" Type="Str">DE_COM_</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Documentation Editor Main.vi</Property>
				<Property Name="Source[1].newName" Type="Str">DE_CORE_Main.vi</Property>
				<Property Name="Source[1].properties[0].type" Type="Str">Run when opened</Property>
				<Property Name="Source[1].properties[0].value" Type="Bool">true</Property>
				<Property Name="Source[1].properties[1].type" Type="Str">Allow debugging</Property>
				<Property Name="Source[1].properties[1].value" Type="Bool">false</Property>
				<Property Name="Source[1].properties[2].type" Type="Str">Auto error handling</Property>
				<Property Name="Source[1].properties[2].value" Type="Bool">false</Property>
				<Property Name="Source[1].properties[3].type" Type="Str">Password</Property>
				<Property Name="Source[1].properties[3].value" Type="Str">TWFyb25nMjAwMg==</Property>
				<Property Name="Source[1].propertiesCount" Type="Int">4</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[10].Container.applyPassword" Type="Bool">true</Property>
				<Property Name="Source[10].Container.applyPrefix" Type="Bool">true</Property>
				<Property Name="Source[10].Container.applySaveSettings" Type="Bool">true</Property>
				<Property Name="Source[10].itemID" Type="Ref">/My Computer/Help</Property>
				<Property Name="Source[10].newName" Type="Str">DE_CORE_</Property>
				<Property Name="Source[10].properties[0].type" Type="Str">Password</Property>
				<Property Name="Source[10].properties[0].value" Type="Str">TWFyb25nMjAwMg==</Property>
				<Property Name="Source[10].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[10].type" Type="Str">Container</Property>
				<Property Name="Source[11].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[11].itemID" Type="Ref">/My Computer/Project Explorer XControl.xctl</Property>
				<Property Name="Source[11].Library.atomicCopy" Type="Bool">true</Property>
				<Property Name="Source[11].newName" Type="Str">DE_PEX.xctl</Property>
				<Property Name="Source[11].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[11].type" Type="Str">Library</Property>
				<Property Name="Source[12].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[12].Container.applyPassword" Type="Bool">true</Property>
				<Property Name="Source[12].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[12].itemID" Type="Ref">/My Computer/Project Explorer XControl.xctl/Abilities</Property>
				<Property Name="Source[12].properties[0].type" Type="Str">Password</Property>
				<Property Name="Source[12].properties[0].value" Type="Str">TWFyb25nMjAwMg==</Property>
				<Property Name="Source[12].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[12].type" Type="Str">Container</Property>
				<Property Name="Source[13].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[13].Container.applyPassword" Type="Bool">true</Property>
				<Property Name="Source[13].Container.applyPrefix" Type="Bool">true</Property>
				<Property Name="Source[13].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[13].itemID" Type="Ref">/My Computer/Project Explorer XControl.xctl/TypeDefs</Property>
				<Property Name="Source[13].newName" Type="Str">DE_PEX_</Property>
				<Property Name="Source[13].properties[0].type" Type="Str">Password</Property>
				<Property Name="Source[13].properties[0].value" Type="Str">TWFyb25nMjAwMg==</Property>
				<Property Name="Source[13].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[13].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[13].type" Type="Str">Container</Property>
				<Property Name="Source[14].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[14].Container.applyPassword" Type="Bool">true</Property>
				<Property Name="Source[14].Container.applyPrefix" Type="Bool">true</Property>
				<Property Name="Source[14].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[14].itemID" Type="Ref">/My Computer/Project Explorer XControl.xctl/SubVIs</Property>
				<Property Name="Source[14].newName" Type="Str">DE_PEX_</Property>
				<Property Name="Source[14].properties[0].type" Type="Str">Password</Property>
				<Property Name="Source[14].properties[0].value" Type="Str">TWFyb25nMjAwMg==</Property>
				<Property Name="Source[14].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[14].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[14].type" Type="Str">Container</Property>
				<Property Name="Source[15].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[15].itemID" Type="Ref">/My Computer/Splash Screen.vi</Property>
				<Property Name="Source[15].newName" Type="Str">QSI Documentation Editor.vi</Property>
				<Property Name="Source[15].properties[0].type" Type="Str">Run when opened</Property>
				<Property Name="Source[15].properties[0].value" Type="Bool">true</Property>
				<Property Name="Source[15].properties[1].type" Type="Str">Allow debugging</Property>
				<Property Name="Source[15].properties[1].value" Type="Bool">false</Property>
				<Property Name="Source[15].properties[2].type" Type="Str">Auto error handling</Property>
				<Property Name="Source[15].properties[2].value" Type="Bool">false</Property>
				<Property Name="Source[15].properties[3].type" Type="Str">Password</Property>
				<Property Name="Source[15].properties[3].value" Type="Str">TWFyb25nMjAwMg==</Property>
				<Property Name="Source[15].propertiesCount" Type="Int">4</Property>
				<Property Name="Source[15].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[15].type" Type="Str">VI</Property>
				<Property Name="Source[16].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[16].itemID" Type="Ref"></Property>
				<Property Name="Source[16].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[2].Container.applyPassword" Type="Bool">true</Property>
				<Property Name="Source[2].Container.applyPrefix" Type="Bool">true</Property>
				<Property Name="Source[2].Container.applySaveSettings" Type="Bool">true</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/TypeDefs</Property>
				<Property Name="Source[2].newName" Type="Str">DE_CORE_</Property>
				<Property Name="Source[2].properties[0].type" Type="Str">Password</Property>
				<Property Name="Source[2].properties[0].value" Type="Str">TWFyb25nMjAwMg==</Property>
				<Property Name="Source[2].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[2].type" Type="Str">Container</Property>
				<Property Name="Source[3].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[3].Container.applyPassword" Type="Bool">true</Property>
				<Property Name="Source[3].Container.applyPrefix" Type="Bool">true</Property>
				<Property Name="Source[3].Container.applySaveSettings" Type="Bool">true</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/CustomControls</Property>
				<Property Name="Source[3].newName" Type="Str">DE_CORE_</Property>
				<Property Name="Source[3].properties[0].type" Type="Str">Password</Property>
				<Property Name="Source[3].properties[0].value" Type="Str">TWFyb25nMjAwMg==</Property>
				<Property Name="Source[3].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[3].type" Type="Str">Container</Property>
				<Property Name="Source[4].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[4].Container.applyPassword" Type="Bool">true</Property>
				<Property Name="Source[4].Container.applyPrefix" Type="Bool">true</Property>
				<Property Name="Source[4].Container.applySaveSettings" Type="Bool">true</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/SubVIs</Property>
				<Property Name="Source[4].newName" Type="Str">DE_CORE_</Property>
				<Property Name="Source[4].properties[0].type" Type="Str">Password</Property>
				<Property Name="Source[4].properties[0].value" Type="Str">TWFyb25nMjAwMg==</Property>
				<Property Name="Source[4].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[4].type" Type="Str">Container</Property>
				<Property Name="Source[5].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[5].Container.applyPassword" Type="Bool">true</Property>
				<Property Name="Source[5].Container.applyPrefix" Type="Bool">true</Property>
				<Property Name="Source[5].Container.applySaveSettings" Type="Bool">true</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/Menus</Property>
				<Property Name="Source[5].newName" Type="Str">DE_CORE_</Property>
				<Property Name="Source[5].properties[0].type" Type="Str">Password</Property>
				<Property Name="Source[5].properties[0].value" Type="Str">TWFyb25nMjAwMg==</Property>
				<Property Name="Source[5].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[5].type" Type="Str">Container</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/CustomControls/Adobe Reader Button.ctl</Property>
				<Property Name="Source[6].type" Type="Str">VI</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">3</Property>
				<Property Name="Source[7].itemID" Type="Ref"></Property>
				<Property Name="Source[7].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/Release Notes.pdf</Property>
				<Property Name="Source[8].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/Help/Open Help.vi</Property>
				<Property Name="Source[9].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">17</Property>
			</Item>
		</Item>
	</Item>
</Project>
