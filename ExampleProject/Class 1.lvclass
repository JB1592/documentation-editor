﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="12008004">
	<Property Name="NI.Lib.Description" Type="Str">Example Class. 

There is a special case that arises when trying to save the description of a class.  The save kept giving the &lt;b&gt;error 1562&lt;/b&gt; because it said it was being accessed by two or more application instances.  This was due to the class being owned by the project it belonged to as well as being accessed by the documentation editor.

To fix this I had to obtain the &lt;b&gt;application reference&lt;/b&gt; from the Project Explorer XControl from when the Project was opened.  Then use that &lt;b&gt;application reference&lt;/b&gt; when opening the Class to write the new description.&lt;b&gt; &lt;/b&gt; &lt;b&gt; &lt;/b&gt;©2012, Q Software Innovations, LLC.Written in National Instruments LabVIEW 2012™.</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">%A#!"!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"&lt;2MR%!813:!!O;K$1#V-#WJ",5Q,OPKI&amp;K9&amp;N;!7JA7VI";=JQVBZ"4F%#-ZG/O26X_ZZ$/87%&gt;M\6P%FXB^VL\_NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAO_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y![_ML^!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">302022660</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">%A#!"!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!".Q5F.31QU+!!.-6E.$4%*76Q!!$TQ!!!1U!!!!)!!!$RQ!!!!5!!!!!1^$&lt;'&amp;T=S!R,GRW9WRB=X-!!!#)%A#!"!!Q!!!I!!!!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!0TK-\.&amp;J2&amp;-G=UUBV1.OP-!!!!-!!!!%!!!!!$^X&gt;$0YH)12YS4Q6]6'07=V"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!PFSGM/J0ME#"8$V9R[ZZLA!!!"!&amp;8?;Y%Z!@B2R,])0JOUBM!!!!%!^H4]G/\C'I.',MK/AT1FA!!!!1-&amp;]M+_':.$$AA)'"Z&lt;Z(0A!!!%=!!5R71U-&lt;1WRB=X-A-3ZM&gt;G.M98.T/E.M98.T)$%O9X2M!!!!!!!"!!*735R#!!!!!!!!5&amp;2)-!!!!!5!!1!"!!!!!!)!!Q!!!!!#!!%!!!!!!"Q!!!!1?*RD9'.A%G!19,2A%'!S9'"A!!!%VQ#E!!!!3!!!!32YH'.AQ!4`A1")-4)Q-$U!UCRIYG!;RK9W1$98GRWY\!7*-T.!(-M+&amp;W5%KG7[!G3!J*B1^9,%Q@Z$!7R9T!9!B!!I;1!!!#I!!6:*2&amp;-&lt;1WRB=X-A-3ZM&gt;G.M98.T/E.M98.T)$%O9X2M!!!!!!!!!!-!!!!!!/]!!!&amp;Y?*S\Q-$!E'FM93&lt;!S-$!$-1AE*S@EMI%J*EB8)95K$A)/%.JQQ/(Q42-4`-&lt;HGY8&amp;9HG(WT],1IA`A]G`F:*)!-I+N,NIS,1,&gt;DJIM,3S;0S8[DZ#-^BQQ-AEI(B-1.-0QN1*5&gt;$B4Q$@[M95/"YAQ05[%Y7F2&gt;A*5&gt;9Q.J9$C/Z9Q&gt;)%6"!';IYL0E.2X?*CE#H#524.^NR"YU$)0-=2$L!F)-(G(+Z!.*^!7QO2^RBJT``'&gt;`]0S7Q(#CAS=$)=!&gt;)+Q.JE,(M$%RAMRE:7"G?-LRB_-TQC_%`!`(!W&gt;`&amp;&amp;:E0#E]!RI^%-!!!!!!/%A'!#!!!"D%S,D!O-1!!!!!!!!Q3!)!%!!!%-4)O-!!!!!!/%A'!#!!!"D%S,D!O-1!!!!!!!!Q3!)!%!!!%-4)O-!!!!!!/%A'!#!!!"D%S,D!O-1!!!!!!!"1"!!!!^6=VAHEGIIQO=V*/"EEZH1!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!QA!!!,Z$&lt;'&amp;T=S".:7VC:8)A2'&amp;U93"$&lt;WZU=G^M)'FT)'FO9WRV:'6E)'FO)'%A9WRB=X-A:GFM:3"C&gt;81A=X2J&lt;'QA;'&amp;T)'FU=S"P&gt;WYA:'6T9X*J=(2J&lt;WYO$4RC0C!],W)_)$RC0C!],W)_$;ES-$%S,#"2)&amp;.P:H2X98*F)%FO&lt;G^W982J&lt;WZT,#"-4%-O$1V8=GFU&gt;'6O)'FO)%ZB&gt;'FP&lt;G&amp;M)%FO=X2S&gt;7VF&lt;H2T)%RB9F:*26=A-D!R-JEO!!!!!!#!`````Y!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A'!!!9'9!!''"A!"G!'!!;!!1!'Q!-!"L!.!!;-/Q!'A^5!"I#L!!;!V1!'A+M!"I$6!!;!KQ!'A.5!"G#O!!99W!!'"O!!"A'!!!@````]!!!1!````````````````````````````````````````````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!"15!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!";V:A[U&amp;!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!";V:,S]P,Y/N"1!!!!!!!!!!!!!!!!!!!!$``Q!!";V:,S]P,S]P,S_$L15!!!!!!!!!!!!!!!!!!0``!).:,S]P,S]P,S]P,S]PA[U!!!!!!!!!!!!!!!!!``]!76EP,S]P,S]P,S]P,S`_AQ!!!!!!!!!!!!!!!!$``Q":AY.:,S]P,S]P,S`_`PZ:!!!!!!!!!!!!!!!!!0``!&amp;G$AY/$73]P,S`_`P\_`FE!!!!!!!!!!!!!!!!!``]!79/$AY/$AVGN`P\_`P\_71!!!!!!!!!!!!!!!!$``Q":AY/$AY/$A`\_`P\_`PZ:!!!!!!!!!!!!!!!!!0``!&amp;G$AY/$AY/$`P\_`P\_`FE!!!!!!!!!!!!!!!!!``]!79/$AY/$AY0_`P\_`P\_71!!!!!!!!!!!!!!!!$``Q":AY/$AY/$A`\_`P\_`PZ:!!!!!!!!!!!!!!!!!0``!&amp;G$AY/$AY/$`P\_`P\_`FE!!!!!!!!!!!!!!!!!``]!AY/$AY/$AY0_`P\_`P[$AQ!!!!!!!!!!!!!!!!$``Q!!76G$AY/$A`\_`P[$L6E!!!!!!!!!!!!!!!!!!0``!!!!!&amp;G$AY/$`P[$AVE!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!":AY/$AS]!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!73]!!!!!!!!!!!!!!!!!!!!!!!!!!0```````````````````````````````````````````Q!!!#I!!5:13&amp;!&lt;1WRB=X-A-3ZM&gt;G.M98.T/E.M98.T)$%O9X2M!!!!!!!!!!-!!!!!!HQ!!!4D?*SNF-^L%U%5R^_%L5R#CJ/EGC[U**:N,7*!&amp;,5'@R1\&amp;3KF+,(A)7!7.`[!VEC3KK@WMJ==?B)C&amp;(LMYMV$$LFYEO"F$`;E9!_R_1O]&amp;!7\W&lt;[&gt;&gt;(?4&amp;O0&amp;82C7:4\P__:^PQS!V'4$A4;M7E$9(H\-7R$34!,13&amp;%Y@.AC_1.E3#974..&amp;NB.IER%,QJIZ13`Q#PT%L@90?Q0?E4$&lt;R;UHG)S61B;=V-RYZ+\3:-K(%;5SY*;-19+NEX&lt;AI:,Y2&gt;@V!KK"0OGME22J!_&amp;H*%E@O[?_S/O+]T?9IL)I'&lt;3!=8/QK$409E75`C2+%B5-5H2,!J;=B&amp;KNZE/2$D1BWJBS'!0,VIH;BYFS=\3E.-],*C19V+G\/DT2@OPU\E"(U2AX';,)P8'0&lt;,B;0'&amp;.#[\6;C'([S(XT))BJ8G&lt;SH28_TYQ?&amp;_P!Q(3?%4N&lt;`:\BY]M/#[)P6%U9DR.7!;`-R;=U]V!DEGOVGP9%D:)LAX8(2PO#"O#P-,!^W(L(TZQK4%W/L/U5CLHC]H#E_4D*&lt;653LYM0H_FFP.*43WLRRW[Q5XZEH.[2US%!_)A11FSX&gt;-OA'%9/!"=@@1GIH'F[8%R=2L.G\DG4^R2^3&gt;X#S@(@T^Y[ERP0"U1A15`KB=RKB&amp;WN*O?K&amp;\_`V'^AAF;02*6S/+\X#&gt;W6TN16V3T3#V$NA]TB=R;4V3&amp;CKP4,[L8DE=V[WJZ5;V7KTU=$DTN265CJ".6@&gt;P?N`=RM$B?#8+1%Y'&amp;Y%*8ZX4H]YJXL&gt;#-QGUK`XU$GU7R.1N/]:HI&amp;T;(V=*UDM[S4816\[,4&gt;*0O.&lt;Z[&gt;U^DXC0V&lt;@B);MJ'AAY@!/)&amp;:R!!!!!%!!!!)!!!!#I!!5*%3&amp;!&lt;1WRB=X-A-3ZM&gt;G.M98.T/E.M98.T)$%O9X2M!!!!!!!!!!-!!!!!!')!!!"S?*RD9'$)%Z"A_M&gt;1^Z?"3?!LE#(^FY&amp;:U)`R.Q-$JZ`!93$.+#!*&amp;*&lt;^S]!OK!U7VD[CS]%!";JMD"S3()=&amp;/=!S(#U;$0```_@Y?O1;8-52(TB4::9]BQ1!&amp;'):!!!!!!!!"!!!!!=!!!'5!!!!"A!!!#&amp;@&lt;GF@4'&amp;T&gt;%NO&lt;X&gt;O4X&gt;O;7ZH4&amp;:$&lt;'&amp;T=U.M&gt;8.U:8)3!)!%!!!!!1!)!$$`````!!%!!!!!!!Y!!!!"!!9!5!!!!!%!!!!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'&amp;C4X*E:8)3!)!%!!!!!A!&amp;!!=!!!Q!1!!"`````Q!!!!%!!1!!!!!!!!!!!!!!'UR71WRB=X.1=GFW982F2'&amp;U962J&lt;76T&gt;'&amp;N=")!A!1!!!!"!!5!"Q!!!1!!S_1J]1!!!!!!!!!G4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B4'&amp;T&gt;%&amp;Q='RJ:725;7VF=X2B&lt;8!3!)!%!!!!!1!&amp;!!=!!!%!!-PE+@%!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962Z='6%:8.D%A#!"!!!!!%!#!!Q`````Q!"!!!!!!!/!!!!!1!'!&amp;!!!!!"!!!!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:2)!A!1!!!!"!!5!!Q!!!1!!!!!!!!!!!!!!!!!%!!)!#!!!!!1!!!"!!!!!+!!!!!)!!!1!!!!!-A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$D!!!"98C=D:".3A."%)7`4C&gt;/-D%G`CW&amp;8LD1D3#ZQ%$!62&lt;"%TBE:E2I'-FUAEPPZT7]109O@*V-=/%G0+C@6V7PKBOY:M\W[`N_#X4'-Z]XD8N]]*NFD);(@"E]&gt;^E#&lt;G:_X92SZ?L+\6L=__JNEY@3&amp;8H)[1GE&amp;'"_-%JMGT*6W/&gt;&amp;U;G9"2W[+N]?I4H)%GTF8_FHH\:_$HOCL#MGGI[K!V+\,CL3K'&lt;$2^&amp;O''J84V\G*"J$?U+8*T5YTBC,D*B)[!$4YD`T6TH@9@]UQY55U2H[1GV+'((*F8TS#X&lt;--_1!!!!!:1!"!!)!!Q!%!!!!3!!0"!!!!!!0!.A!V1!!!&amp;%!$Q1!!!!!$Q$9!.5!!!";!!]%!!!!!!]!W!$6!!!!9Q!3B!#!!!!2!/]!Z!B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631%Q!!!!5F.31QU+!!.-6E.$4%*76Q!!$TQ!!!1U!!!!)!!!$RQ!!!!!!!!!!!!!!#!!!!!U!!!%+!!!!"R-35*/!!!!!!!!!7"-6F.3!!!!!!!!!8236&amp;.(!!!!!!!!!9B01F.(!!!!!!!!!:R$1V.(!!!!!!!!!&lt;"-38:J!!!!!!!!!=2$4UZ1!!!!!!!!!&gt;B544AQ!!!!!!!!!?R%2E24!!!!!!!!!A"-372T!!!!!!!!!B2735.%!!!!!!!!!CBW:8*T!!!!"!!!!DR41V.3!!!!!!!!!K"(1V"3!!!!!!!!!L246&amp;*(!!!!!!!!!MB*1U^/!!!!!!!!!NRJ9WQY!!!!!!!!!P"-37:Q!!!!!!!!!Q2'5%BC!!!!!!!!!RB'5&amp;.&amp;!!!!!!!!!SR-37*E!!!!!!!!!U"#2%BC!!!!!!!!!V2#2&amp;.&amp;!!!!!!!!!WB73624!!!!!!!!!XR%6%B1!!!!!!!!!Z".65F%!!!!!!!!![2)36.5!!!!!!!!!\B71V21!!!!!!!!!]R'6%&amp;#!!!!!!!!!_!!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"A!!!!!!!!!!0````]!!!!!!!!!J!!!!!!!!!!!`````Q!!!!!!!!#Y!!!!!!!!!!$`````!!!!!!!!!-Q!!!!!!!!!!0````]!!!!!!!!!Y!!!!!!!!!!!`````Q!!!!!!!!%M!!!!!!!!!!$`````!!!!!!!!!41!!!!!!!!!!0````]!!!!!!!!"6!!!!!!!!!!!`````Q!!!!!!!!'A!!!!!!!!!!$`````!!!!!!!!!&gt;!!!!!!!!!!"0````]!!!!!!!!#R!!!!!!!!!!(`````Q!!!!!!!!,9!!!!!!!!!!D`````!!!!!!!!!OA!!!!!!!!!#@````]!!!!!!!!#`!!!!!!!!!!+`````Q!!!!!!!!--!!!!!!!!!!$`````!!!!!!!!!S!!!!!!!!!!!0````]!!!!!!!!$/!!!!!!!!!!!`````Q!!!!!!!!.-!!!!!!!!!!$`````!!!!!!!!""1!!!!!!!!!!0````]!!!!!!!!%G!!!!!!!!!!!`````Q!!!!!!!!C=!!!!!!!!!!$`````!!!!!!!!#-Q!!!!!!!!!!0````]!!!!!!!!,4!!!!!!!!!!!`````Q!!!!!!!!N5!!!!!!!!!!$`````!!!!!!!!#Y1!!!!!!!!!!0````]!!!!!!!!,\!!!!!!!!!!!`````Q!!!!!!!!PU!!!!!!!!!!$`````!!!!!!!!$9Q!!!!!!!!!!0````]!!!!!!!!.F!!!!!!!!!!!`````Q!!!!!!!!W=!!!!!!!!!!$`````!!!!!!!!$=A!!!!!!!!!A0````]!!!!!!!!/M!!!!!!,1WRB=X-A-3ZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!1^$&lt;'&amp;T=S!R,GRW9WRB=X.16%AQ!!!!!!!!!!!!!!!"!!%!!!!!!!!!!!!!!1!'!&amp;!!!!!"!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"%!A!A!!!!!!!!!!!!!</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Item Name="Class 1.ctl" Type="Class Private Data" URL="Class 1.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="New Folder" Type="Property Definition">
		<Property Name="NI.ClassItem.Property.LongName" Type="Str">New Folder</Property>
		<Property Name="NI.ClassItem.Property.ShortName" Type="Str">New Folder</Property>
		<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
		<Item Name="Untitled 1.vi" Type="VI" URL="../Untitled 1.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%A#!"!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%*!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#:!=!!?!!!2$U.M98.T)$%O&lt;(:D&lt;'&amp;T=Q!,1WRB=X-A-3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!G1(!!(A!!%1^$&lt;'&amp;T=S!R,GRW9WRB=X-!#E.M98.T)$%A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
		</Item>
		<Item Name="Untitled 5.vi" Type="VI" URL="../Untitled 5.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%A#!"!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%7!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#:!=!!?!!!2$U.M98.T)$%O&lt;(:D&lt;'&amp;T=Q!,1WRB=X-A-3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!G1(!!(A!!%1^$&lt;'&amp;T=S!R,GRW9WRB=X-!#E.M98.T)$%A;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
		</Item>
	</Item>
	<Item Name="Control 2.ctl" Type="VI" URL="../Control 2.ctl">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%A#!"!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">4194312</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
	</Item>
	<Item Name="Untitled 2.vi" Type="VI" URL="../Untitled 2.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%A#!"!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%*!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#:!=!!?!!!2$U.M98.T)$%O&lt;(:D&lt;'&amp;T=Q!,1WRB=X-A-3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!G1(!!(A!!%1^$&lt;'&amp;T=S!R,GRW9WRB=X-!#E.M98.T)$%A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
	</Item>
	<Item Name="Untitled 4.vi" Type="VI" URL="../Untitled 4.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%A#!"!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%7!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#:!=!!?!!!2$U.M98.T)$%O&lt;(:D&lt;'&amp;T=Q!,1WRB=X-A-3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!G1(!!(A!!%1^$&lt;'&amp;T=S!R,GRW9WRB=X-!#E.M98.T)$%A;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
	</Item>
</LVClass>
